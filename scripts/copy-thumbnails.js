import sh from 'shelljs'

const notIndexOrUnderscore = fileName => (
  !fileName.startsWith('_') && fileName !== 'index.svx'
)

const EXAMPLES_PATH = 'src/routes/examples'
const THUMBNAILS_PATH = 'static/thumbnails'

sh.mkdir('-p', THUMBNAILS_PATH)

sh.ls(EXAMPLES_PATH)
  .filter(notIndexOrUnderscore)
  .forEach(folderName => {
    const targetFile = `${THUMBNAILS_PATH}/${folderName}.png`

    if (sh.test('-f', targetFile)) return;

    sh.cp(
      `${EXAMPLES_PATH}/${folderName}/_thumbnail.png`,
      `${THUMBNAILS_PATH}/${folderName}.png`
    )
  })