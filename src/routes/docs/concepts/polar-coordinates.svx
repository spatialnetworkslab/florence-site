<script>
  import SimplePolarExample from './examples/_SimplePolarExample.svelte'
  import PolarShowcase from './examples/_PolarShowcase.svelte'
  import PolarScalingShowcase from './examples/_PolarScalingShowcase.svelte'
  import PolarLineVsRect from './examples/_PolarLineVsRect.svelte'
</script>

# Polar coordinates

<SimplePolarExample />

```svelte
<script>
  import { Graphic, polar, RectangleLayer } from '@snlab/florence'
</script>

<Graphic width={200} height={200} coordinates={polar()}>
  <RectangleLayer
    x1={[0, 0.3, 0.7, 0.85]}
    x2={[0.3, 0.7, 0.85, 1]}
    fill={['#c84669', '#388077', '#92d82b', '#20609b']}
  />
</Graphic>
```

## Arguments

The `polar` function takes the following arguments:

| Prop       | Required | Types    | Default         | Unit(s) |
| ---------- | -------- | -------- | --------------- | ------- |
| startAngle | `false`  | `number` | `-Math.PI / 2`  | Radian  |
| endAngle   | `false`  | `number` | `1.5 * Math.PI` | Radian  |

## Usage

In polar coordinate systems usually encountered in mathematics, the coordinate system begins on the right and turns counterclockwise. In `florence`, polar coordinates start at the top of the `Section` due to the `startAngle` being `-0.5 PI`, and turn clockwise because of the nature of coordinate systems on the web (which always go from top to bottom). This makes it easy to create pie charts, which also start from the top and turn clockwise. To make polar coordinates behave like in mathematics, the following settings can be used:

```svelte
<Section
  coordinates={polar({ startAngle: 0, endAngle: 2 * Math.PI })}
  flipY
>
```

Check out the following code and the resulting output, and try switching the transformation from identity to polar:

```svelte
<script>
  import { 
    Graphic, Rectangle, LineLayer, LabelLayer, cartesian, polar
  } from '@snlab/florence'

  let transformation = 'identity'
  $: coordinates = transformation === 'identity' ? cartesian() : polar()
</script>

<div>
  <label for="coordinate-select">Coordinates:</label>
  <select name="coordinate-select" bind:value={transformation}>
    <option value="identity">Identity</option>
    <option value="polar">Polar</option>
  </select>
</div>

<Graphic 
  width={500}
  height={500}
  {coordinates}
  backgroundColor="#b2ffb2"
>

  <Rectangle fill="white" opacity={0.4} />

  <LineLayer 
    x={[[0, 0], [0.25, 0.25], [0.5, 0.5], [0.75, 0.75]]}
    y={[[0, 1], [0, 1], [0, 1], [0, 1]]}
    stroke={["red", "orange", "yellow" , "green"]}
  />

  <LabelLayer
    x={[0, 0.25, 0.5, 0.75]}
    y={[0.5, 0.5, 0.5, 0.5]}
    text={[0, 0.25, 0.5, 0.75]}
    fontSize={23}
  />

</Graphic>
```

<PolarShowcase />

Polar coordinates can be used either with or without scales. In the examples above, no scales are being used. But combining a polar transformation with scaling works fine too:

```svelte
...
<Graphic 
  width={500}
  height={500}
  {coordinates}
  backgroundColor="#b2ffb2"
  scaleX={scaleLinear().domain([0, 4])}
  scaleY={scaleLinear().domain([0, 4])}
>

  <Rectangle fill="white" opacity={0.4} />

  <LineLayer 
    x={[[0, 0], [1, 1], [2, 2], [3, 3]]}
    y={[[0, 4], [0, 4], [0, 4], [0, 4]]}
    stroke={["red", "orange", "yellow" , "green"]}
  />

  <LabelLayer
    x={[0, 1, 2, 3]}
    y={[2, 2, 2, 2]}
    text={[0, 1, 2, 3]}
    fontSize={23}
  />

</Graphic>
```

<PolarScalingShowcase />

## Reprojection of vertices vs segments

For most marks and layers, only the vertices are reprojected to polar coordinates. The exceptions are the `Rectangle` and `RectangleLayer`, for which the line segments between the vertices are also reprojected. Compare the behavior of a `LineLayer` vs a `RectangleLayer` in both cartesian and polar coordinates:

```svelte
<Graphic width={500} height={500} {coordinates} backgroundColor={'#f3f3f3'}>
  <RectangleLayer
    x1={[0, 0.5]}
    x2={[0.25, 0.75]}
    y1={[0, 0]}
    y2={[1, 1]}
    fill={['red', 'blue']}
    opacity={0.2}
  />
  <LineLayer
    x={[[0, 0, 0.25, 0.25, 0], [0.5, 0.5, 0.75, 0.75, 0.5]]}
    y={[[0, 1, 1, 0, 0], [0, 1, 1, 0, 0]]}
    stroke={['red', 'blue']}
    strokeWidth={5}
  />
</Graphic>
```

<PolarLineVsRect />

In a future version, an option will be added to other marks and layers besides `Rectangle` and `RectangleLayer` that will allow you to decide if you want to reproject the line segments as well.
