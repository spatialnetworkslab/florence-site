<script>
  import SimpleExample from './examples/_AreaSimple.svelte'
  import HorizontalArea from './utils/_HorizontalArea.svelte'
  import VerticalArea from './utils/_VerticalArea.svelte'
</script>

# Area

The `Area` mark is used to plot filled areas. It is often used to visualize change over time. It can either be used by itself or in a 'stacked' configuration.

```svelte
<script>
  import { Graphic, Area, AreaLayer, XAxis, YAxis } from '@snlab/florence'
  import { scaleLinear, scaleTime } from 'd3-scale'

  const x = [new Date(2002, 0, 1), new Date(2004, 0, 1), new Date(2006, 0, 1)]
  const yFirst = [20, 40, 10]
  const ySecond = [30, 45, 30]
  const yThird = [35, 60, 55]
</script>

<Graphic 
  width={200} height={200}
  scaleX={scaleTime().domain([new Date(2001, 0, 1), new Date(2007, 0, 1)])}
  scaleY={scaleLinear().domain([0, 60])}
  padding={20}
  flipY
>

  <Area x={x} y1={[0, 0, 0]} y2={yFirst} fill={'red'} />
  <AreaLayer x1={[x, x]} y1={[yFirst, ySecond]} y2={[ySecond, yThird]} fill={['blue', 'green']} />
  
  <XAxis />
  <YAxis />

</Graphic>
```

<SimpleExample />

## Properties

### Positioning

| Prop  | Required                                    | Type(s)            | Default     | Unit(s)                                                   |
| ----- | ------------------------------------------- | ------------------ | ----------- | --------------------------------------------------------- |
| x     | `if (y1 !== undefined && y2 !== undefined)` | `any[] | Function` | `undefined` | [Local coordinate](/docs/concepts/local-coordinates)      |
| y     | `if (x1 !== undefined && x2 !== undefined)` | `any[] | Function` | `undefined` | [Local coordinate](/docs/concepts/local-coordinates)      |
| x1    | `if (y !== undefined)`                      | `any[] | Function` | `undefined` | [Local coordinate](/docs/concepts/local-coordinates)      |
| x2    | `if (y !== undefined)`                      | `any[] | Function` | `undefined` | [Local coordinate](/docs/concepts/local-coordinates)      |
| y1    | `if (x !== undefined)`                      | `any[] | Function` | `undefined` | [Local coordinate](/docs/concepts/local-coordinates)      |
| y2    | `if (x !== undefined)`                      | `any[] | Function` | `undefined` | [Local coordinate](/docs/concepts/local-coordinates)      |
| curve | `false`                                     | `D3ShapeCurve`     | `undefined` | A [d3-shape curve](https://github.com/d3/d3-shape#curves) |

The `Area` is defined by the region between two bounding polylines, which are like Line marks defined with `x1` `y1` and `x2` `y2` props respectively. How an area is bounded (its 'orientation') is determined by which props are combined. When `x` is defined, `y1` and `y2` are required, and the `Area` is oriented horizontally. Likewise, When `y` is defined, `x1` and `x2` are required, and the `Area` will be oriented vertically. This is illustrated by the examples below:

<HorizontalArea />
<VerticalArea />

All props accepts `Array`s of any type (`any[]`), depending on the scales used in the parent [Graphic](/docs/core/graphic), [Section](/docs/core/section) or [Glyph](/docs/core/glyph). Any prop also accepts a `Function` that returns an `Array` of default coordinates. Instead of `Array`s (`any[]`), the `AreaLayer` accepts only `Array`s of `Array`s (`any[][]`), or `Function`s that return `Array`s of `Array`s of default coordinates.

### Aesthetics

| Prop          | Required | Type(s)                       | Default     | Unit(s)                                                                                                                          |
| ------------- | -------- | ----------------------------- | ----------- | -------------------------------------------------------------------------------------------------------------------------------- |
| stroke        | `false`  | `string`                      | `'none'`    | Named, hex, rgb or hsl color                                                                                                     |
| strokeWidth   | `false`  | `number`                      | `0`         | Pixel                                                                                                                            |
| strokeOpacity | `false`  | `number`                      | `undefined` | Number between 0 and 1                                                                                                           |
| fill          | `false`  | `string`                      | `'#000000'` | Named, hex, rgb or hsl color                                                                                                     |
| fillOpacity   | `false`  | `number`                      | `undefined` | Number between 0 and 1                                                                                                           |
| opacity       | `false`  | `number`                      | `1`         | Number between 0 and 1                                                                                                           |
| lineCap       | `false`  | `'butt' | 'round' | 'square'` | `'butt'`    | -                                                                                                                                |
| lineJoin      | `false`  | `'bevel' | 'round' | 'miter'` | `'bevel'`   | -                                                                                                                                |
| miterLimit    | `false`  | `number`                      | `10`        | Pixel                                                                                                                            |
| dashArray     | `false`  | `string`                      | `undefined` | A string of numbers seperated by spaces. See [here](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/stroke-dasharray) |
| dashOffset    | `false`  | `number`                      | `undefined` | Pixel                                                                                                                            |

These aesthetic props are similar to attributes of the SVG [path](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/path) element. The analogous `path` attributes are shown below in brackets:

- `fill` refers to the color of the polygon (`fill`)
- `fillOpacity` refers to the transparency of the polygon, `0` being fully transparent (`fill-opacity`)
- `stroke` refers to the color of the polygon's outline or 'stroke' (`stroke`)
- `strokeWidth` refers to the width of the polygon's outline or 'stroke' (`stroke-width`)
- `strokeOpacity` refers to the transparency of the polygon's outline or 'stroke', `0` being fully transparent (`stroke-opacity`)
- `opacity` sets both the `strokeOpacity` and the `fillOpacity` (`opacity`)
- `lineCap` controls the endings of strokes (in this case only relevant when `dashArray` is used) (`stroke-linecap`)
- `lineJoin` controls the shape to be used at the corners of the stroke (`stroke-linejoin`)
- `miterLimit`, if `lineJoin` is `'miter'`, sets a limit on how long the miter can be (`stroke-miterlimit`)
- `dashArray` can be used to make dashed strokes (`stroke-dasharray`)
- `dashOffset` shifts the pattern defined in `dashArray` by a number of pixels (`stroke-dashoffset`)

### Interactions

#### Mouse events

| Prop        | Required | Type(s)    | Default     | Unit(s) |
| ----------- | -------- | ---------- | ----------- | ------- |
| onClick     | `false`  | `Function` | `undefined` | -       |
| onMousedown | `false`  | `Function` | `undefined` | -       |
| onMouseup   | `false`  | `Function` | `undefined` | -       |
| onMouseover | `false`  | `Function` | `undefined` | -       |
| onMouseout  | `false`  | `Function` | `undefined` | -       |
| onMousedrag | `false`  | `Function` | `undefined` | -       |

#### Touch events

| Prop        | Required | Type(s)    | Default     | Unit(s) |
| ----------- | -------- | ---------- | ----------- | ------- |
| onTouchdown | `false`  | `Function` | `undefined` | -       |
| onTouchup   | `false`  | `Function` | `undefined` | -       |
| onTouchover | `false`  | `Function` | `undefined` | -       |
| onTouchout  | `false`  | `Function` | `undefined` | -       |
| onTouchdrag | `false`  | `Function` | `undefined` | -       |

#### Select events

| Prop       | Required | Type(s)    | Default     | Unit(s) |
| ---------- | -------- | ---------- | ----------- | ------- |
| onSelect   | `false`  | `Function` | `undefined` | -       |
| onDeselect | `false`  | `Function` | `undefined` | -       |

See the [interactivity](/docs/concepts/interactivity) documentation for more information.

### Other

| Prop            | Required | Type(s)                           | Default     | Unit(s) |
| --------------- | -------- | --------------------------------- | ----------- | ------- |
| outputSettings  | `false`  | `OutputSettings`                  | `undefined` | -       |
| clip            | `false`  | `'padding' | 'outer' | undefined` | `'outer'`   | -       |

- `outputSettings`: see the [advanced rendering](/docs/concepts/advanced-rendering) documentation for more information.
- `clip`: if defined, overrides the clip mode of the parent `Graphic`, `Section` or `Glyph`. 

In addition, the `AreaLayer` has the following props:

| Prop      | Required | Type(s)    | Default     | Unit(s)              |
| --------- | -------- | ---------- | ----------- | -------------------- |
| keys      | `false`  | `string[]` | `undefined` | Array of unique keys |
| asOnePath | `false`  | `boolean`  | `false`     | -                    |

See the [marks versus layers](/docs/concepts/marks-vs-layers) documentation for more information on how `keys` and `asOnePath` work.

## Examples

The `Area` and `AreaLayer` can be seen in action in the following examples:

- [Area Chart](/examples/area-chart)
- [Stacked Area Chart](/examples/stacked-area)
- [Violin Plot](/examples/violin-plot)
