export const posts = [
  {
    title: "Core components",
    path: "/docs/core/graphic",
    children: [
      {
        title: "Graphic",
        path: "/docs/core/graphic",
      },
      {
        title: "Section",
        path: "/docs/core/section",
      },
      {
        title: "Grid",
        path: "/docs/core/grid",
      },
      {
        title: "Glyph",
        path: "/docs/core/glyph"
      }
    ],
  },

  {
    title: "Concepts",
    path: "/docs/concepts/local-coordinates",
    children: [
      {
        title: "Local coordinates",
        path: "/docs/concepts/local-coordinates",
      },
      {
        title: "Polar coordinates",
        path: "/docs/concepts/polar-coordinates"
      },
      {
        title: "Marks versus Layers",
        path: "/docs/concepts/marks-vs-layers",
      },
      {
        title: "Interactivity",
        path: "/docs/concepts/interactivity",
      },
      {
        title: "Zooming and panning",
        path: "/docs/concepts/zoom-pan",
      },
      {
        title: "Advanced rendering",
        path: "/docs/concepts/advanced-rendering",
      },
    ],
  },
  {
    title: "Marks",
    path: "/docs/marks/point",
    children: [
      {
        title: "Point",
        path: "/docs/marks/point",
      },
      {
        title: "Symbol",
        path: "/docs/marks/symbol",
      },
      {
        title: "Line",
        path: "/docs/marks/line",
      },
      {
        title: "Rectangle",
        path: "/docs/marks/rectangle",
      },
      {
        title: "Area",
        path: "/docs/marks/area",
      },
      {
        title: "Label",
        path: "/docs/marks/label",
      },
      {
        title: "Polygon",
        path: "/docs/marks/polygon",
      },
      {
        title: "FuncLine",
        path: "/docs/marks/funcline",
      },
    ],
  },
  {
    title: "Guides",
    path: "/docs/guides/xaxis",
    children: [
      {
        title: "X-axis",
        path: "/docs/guides/xaxis",
      },
      {
        title: "Y-axis",
        path: "/docs/guides/yaxis",
      },
      {
        title: "X-gridlines",
        path: "/docs/guides/xgridlines",
      },
      {
        title: "Y-gridlines",
        path: "/docs/guides/ygridlines",
      },
      {
        title: "Discrete legend",
        path: "/docs/guides/discrete-legend",
      },
      // {
      //   title: "Gradient legend",
      //   path: "/docs/guides/gradient-legend",
      // },
    ],
  },
  {
    title: "Helpers",
    path: "/docs/helpers/fit-scales",
    children: [
      {
        title: "fitScales",
        path: "/docs/helpers/fit-scales",
      },
      {
        title: "getClassLabels",
        path: "/docs/helpers/get-class-labels",
      },
      {
        title: "identity",
        path: "/docs/helpers/identity"
      }
    ],
  },
];