import { posts } from './_content-structure.js'

const contents = JSON.stringify(posts)

export function get () {
  return {
    status: 200,
    headers: {'content-type': 'application/json'},
    body: contents
  }
}
