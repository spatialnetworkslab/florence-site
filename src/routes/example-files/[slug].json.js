import fs from 'fs'

const cache = new Map()

export function get({ params: { slug } }) {
  if (!cache.has(slug)) {
    cache.set(slug, getExample(slug))
  }

  return {
    body: cache.get(slug)
  }
}

const BASE_PATH = 'src/routes/examples'

const appFirst = (a, b) => {
  if (a.name === 'App' && a.type === 'svelte') return -1
  if (b.name === 'App' && b.type === 'svelte') return 1

  return 0
}

function getExample (slug) {
  const folderPath = `${BASE_PATH}/${slug}/_files`

  const files = fs.readdirSync(folderPath)
    .map((fileName) => {
      const [name, type] = splitFileName(fileName)
      const source = fs.readFileSync(`${folderPath}/${fileName}`, 'utf-8')

      return {
        name,
        type,
        source
      }
    })

  return files
    .sort(appFirst)
    .map((file, i) => ({ id: i, ...file }))
}

function splitFileName (fileName) {
  const split = fileName.split('.')
  const splitLength = split.length

  if (splitLength === 1) return [fileName, 'txt']
  if (splitLength === 2) return split

  const lastIndex = splitLength - 1

  return [
    split.slice(0, lastIndex).join('.'),
    split[lastIndex]
  ]
}
