export default [
  '<script>',
  '  import { Graphic, Point } from "@snlab/florence"',
  '</script>',
  '',
  '<Graphic width={400} height={400}>',
  '  <Point x={0.5} y={0.5} radius={50} />',
  '</Graphic>'
].join('\n')
