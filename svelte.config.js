import adapter from '@sveltejs/adapter-static';
import preprocess from "svelte-preprocess";
import containers from "remark-containers";
import { mdsvex } from "mdsvex";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    // hydrate the <div id="svelte"> element in src/app.html
    target: "#svelte",
    prerender: { onError: "continue" },
    adapter: adapter({
			pages: 'build',
			assets: 'build',
			fallback: null
		})
  },
  extensions: [".svelte", ".svx"],
  preprocess: [
    mdsvex({
      remarkPlugins: [containers]
    }),
    preprocess({
      postcss: true
    }),
  ],
};

export default config;
