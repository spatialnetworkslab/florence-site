const config = {
  mode: "jit",
  purge: ["./src/**/*.{html,js,svelte,ts,svx}"],

  theme: {
    extend: {},
  },

  plugins: [],
};

module.exports = config;
